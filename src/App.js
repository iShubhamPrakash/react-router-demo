import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './Home'
import About from './About'
import Contact from './Contact'

import {Link, Route} from 'react-router-dom'


const Nav=()=>(
  <div>
    <ul>
      <li><Link to="/">Home</Link></li>
      <li><Link to="/about">About</Link></li>
      <li><Link to="/contact">Contact</Link></li>
    </ul>
  </div>
)

class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav/>
        <Route exact path="/" component={Home}/> 
        <Route path="/about" component={About}/> 
        <Route path="/contact" component={Contact}/> 
      </div>
    );
  }
}

export default App;
